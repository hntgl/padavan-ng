DIR="out"
DL_NAME="mipsel-linux-uclibc.tar.xz"
DL_URL="https://github.com/hntgl/mipsel-linux-uclibc/releases/download/ffc2e62//$DL_NAME"

if [ -d $DIR ]; then
	echo "$DIR exists!"
	exit
fi

curl -O -L $DL_URL && \
mkdir -p $DIR && \
tar -xvf $DL_NAME -C $DIR
